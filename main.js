const { Client, MessageEmbed } = require("discord.js");
const { TOKEN, PREFIX } = require("./config");
const client = new Client({ disableEveryone: true });


client.on("message", msg => {
  if (msg.author.bot) return;
  if (msg.content.indexOf(PREFIX) !== 0) return;
  const args = msg.content.slice(PREFIX.length).trim().split(/ +/g);
  const cmd = args.shift().toLocaleLowerCase();
  console.log(args);
  console.log(cmd);
  if (cmd === "ping") msg.channel.send("pong !");
  if (cmd === "pong") msg.channel.send("ping !");
  if (cmd === "repeat") {
    msg.channel.send(args.join(" "));
    msg.delete({ timeout: 3000 }).then(console.log("le message a été suprimer !"));
  }
  if (cmd === "role") {
    const channel = client.channels.find(r => r.name === "server-log");
    const role = msg.guild.roles.find(r => r.name === args[0]);
    if (!role) return msg.channel.send("Ce rôle n'existe pas !");
    if (msg.member.roles.find(r => r.name === args[0])) {
      msg.member.roles.remove(role);
      channel.send(`J'ai supprimé le role ${role} à ${msg.author}. `);
    } else {
      msg.member.roles.add(role);
      channel.send(`J'ai ajouté le role ${role} à ${msg.author}. `);
    }
  }
  if (cmd === "sinfo") {
    const embed = new MessageEmbed()
      .setDescription(msg.guild.name)
      .setThumbnail(msg.guild.iconURL())
      .addField("membres", msg.guild.memberCount)
      .setFooter(msg.guild.owner.user.tag, msg.guild.owner.user.avatarURL())
      .setTimestamp();
    msg.channel.send(embed);
  }
});

client.on("guildMemberAdd", member => {
  member.send("Salut Gardiens je te souhaite la bienvenue sur notre serveur discord !");
  const channel = client.channels.find(r => r.name === "server-log");
  channel.send(`${member} a rejoint le serveur !`);
});


client.login(TOKEN);

client.on("ready", () => console.log("Je suis prêt !"));
client.on("readdy", console.error);
client.on("warn", console.warn);
client.on("debug", console.log);

